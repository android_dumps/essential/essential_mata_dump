

digit	
digit

letters
	letters
�
decimal�
decimal.negative
decimal.integer_part"WShort
!decimal.fractional_part"WShort
decimal.exponent"WShort
decimal.quantity
�

electronic�
electronic.protocol
electronic.username
electronic.password
electronic.domain
electronic.port
electronic.path
electronic.query_string
electronic.fragment_id
k
fraction_
fraction.integer_part"WShort
fraction.numerator"WShort
fraction.denominator"WShort
�
measure�
measure.decimal.negative
&measure.decimal.integer_part"WShort
)measure.decimal.fractional_part"WShort
"measure.decimal.exponent"WShort
measure.decimal.quantity
measure.unitsmeasure.decimal.integer_part�
'measure.fraction.integer_part"WShort
$measure.fraction.numerator"WShort
&measure.fraction.denominator"WShort
measure.unitsmeasure.fraction.numeratorO
"measure.cardinal.integer"WShort
measure.unitsmeasure.cardinal.integere
measure.unitsmeasure.cardinal.integermeasure.decimal.integer_partmeasure.fraction.numerator
J
ordinal?
=" ordinal.morphosyntactic_featuresordinal.integer"WShort
�
money�
money.style
money.amount.negative
5money.currencymoney.amount.integer_part"WShort
8money.currencymoney.amount.fractional_part"WShortmoney.quantitymoney.string_quantitymoney.amount.exponent�
money.style
money.amount.negative
#money.amount.integer_part"WShort
&money.amount.fractional_part"WShort
money.amount.exponent"WShort
money.quantity
money.string_quantity
money.currency
W
	telephoneJ
telephone.country_code
telephone.number_part
telephone.extension
M
cardinalA
?#!cardinal.morphosyntactic_featurescardinal.integer"WShort
f
time^

time.style

time.hours
time.minutes
time.seconds
time.speak_period
	time.zone
i
datea

date.style
date.weekday
date.day"WShort

date.month
	date.year"WShort

date.era

	connector
connector.type

concept
	concept
)
verbatim

verbatim
concise_emoji
%
abbreviation
abbreviation.text

spell
spell.graphemes